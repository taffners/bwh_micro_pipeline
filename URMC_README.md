# **URMC Micro NGS Pipeline**

# What is it?

	* URMC Micro NGS Pipeline is a python-based software aimed at quickly producing a full work-up of a group of bacterial whole genome NGS data.

# Get started

	* [Sign up for a blue hive account](https://registration.circ.rochester.edu/account)
	* Learn about Linux
		* [Circ's basic Training on linux](https://info.circ.rochester.edu/#Training/Linux.pdf)
		* [Circ hosts work shops and bootcamps](https://www.circ.rochester.edu/events.html)
		* Check out MOOCs for classes that are more in depth these are great resources and are free!
			* [Coursera](https://www.coursera.org/)
			* [edx](https://www.edx.org/)
			* [Khan Academy](https://www.khanacademy.org/)

# To view this file.

	* I use the sublime text editor to view this file. [sublime](https://www.sublimetext.com/)
	* You could convert this file to a PDF using a [markdown converter like this one](https://www.markdowntopdf.com/)

# Illumina Paired end short read analysis from fastq files

	![Alt text](micro_pipeline_simple.png)

	* raw data requirements

		* Two fastq files for each sample.  forward and reverse.
		* Unique names for each sample even repeats.

	* Required flow overview
		* Step 1: put_fastq_in_folders.py

			* Use this to put your output fastq.gz files in project folders 
			* To reduce clutter in the micro_pipeline_v2 folder I run this program at /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data/samples_not_assigned_projects.  This program I thru together quickly and it requires the folder containing all fastq files to be in the same folder that put_fastq_in_folders.py

		* Step 2: micro_pipeline_v2.py 

			* Runs quality stats, makes contigs, finds genome faction of all substrains in species folder, runs analysis to find genus and species, and resistance genes of all input files 

		* Step 3: micro_pipeline_SNP_calling_v2.py

			* Produces phylogenetic analysis

	* project folders use put_fastq_in_folders.py

		* The pipeline requires the data to be stored in a specific folder format. 
		* The folder format is

			* Project folder -> sample folder -> fasta files 
			* Examples can be found at /gpfs/fs2/scratch/acamero7/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data
		
		* Folders should be made using the script put_fastq_in_folders.py
		* Project folders should only have sample folders in them
		* The pipeline will store intermediate files in sample folders to make reanalysis faster.  Do not delete these.

	* Examples: Parts of this example are also explained under Test install and see if you have the correct permissions review both 

		* Example data is located at /gpfs/fs2/scratch/acamero7/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data/samples_not_assigned_projects or at Box\micro\about_micro_pipeline\pipeline_backup\samanthas_last_pipeline_backup\example_data_and_run_files

			* raw data from in house MiSEQ.  Novogene Steps for put_fastq_in_folders.py are slightly different.  Refer to put_fastq_in_folders.py for Novogene

				* I set up a little raw data example set.  This is the data you are looking to transfer to CIRC from the MiSeq output.  As you can see in this example each sample has two files.  We have 2 E_col samples, 1 K_pie sample, and one N_men sample.

					$ cd /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data/samples_not_assigned_projects/example_raw_data/
					$ ls -l
					URMC-774-E-col_examp_S19_L001_R1_001.fastq.gz
					URMC-774-E-col_examp_S19_L001_R2_001.fastq.gz
					URMC-775-E-col_examp_S20_L001_R1_001.fastq.gz
					URMC-775-E-col_examp_S20_L001_R2_001.fastq.gz
					URMC-777-K-pie_examp_S21_L001_R1_001.fastq.gz
					URMC-777-K-pie_examp_S21_L001_R2_001.fastq.gz
					URMC-778-N-men_examp_S22_L001_R1_001.fastq.gz
					URMC-778-N-men_examp_S22_L001_R2_001.fastq.gz

		* Example run Illumina data
			* Make sample folders inside of a project folder put_fastq_in_folders.py
			* Place all fastq files inside of a folder located at /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data/samples_not_assigned_projects
 
			* Display help menu

				$ python put_fastq_in_folders.py -h

			* Put all of these files in a folder.  Note this function will not copy any files it will just move them around.  I made a copy in this example to keep original and result for tutorial use.

				$ python put_fastq_in_folders.py -d example_raw_data
				Making folder of URMC_774_E_col_examp
				Making folder of URMC_775_E_col_examp
				Making folder of URMC_777_K_pie_examp
				Making folder of URMC_774_E_col_examp
				Making folder of URMC_777_K_pie_examp
				Making folder of URMC_778_N_men_examp
				Making folder of URMC_775_E_col_examp
				Making folder of URMC_778_N_men_examp

			* New folder structure example is at /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data/samples_not_assigned_projects/example_after_put_in_folder

				$ ls -1
				URMC_774_E_col_examp
				URMC_775_E_col_examp
				URMC_777_K_pie_examp
				URMC_778_N_men_examp

			* Now manually make project folders based on species example is at /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data/samples_not_assigned_projects/example_project_folders.  Note I made a copy of example_after_put_in_folder and called it example_project_folders just for demo purposes.

				$ cd example_project_folders/
				$ ls -1
				E_col
				K_pie
				N_men

			* Now everything else will be run via the micro_pipeline_v2 folder

				$ cd /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2

			* Run micro_pipeline_v2.py

				* show help menu

					$ python micro_pipeline_v2.py -h

				* In this example I'm only going to run E_col project folder.  The first step is to find species

					$ python micro_pipeline_v2.py -refs

					Current species with references are:
					                Citrobacter_freundii
					                Clostridium_difficile
					                Corynebacterium_diphtheriae
					                Corynebacterium_ulcerans
					                Enterobacter_aerogenes
					                Enterobacter_cloacae
					                Escherichia_coli
					                Facklamia_hominis
					                Human_coxsackievirus
					                Inquilinus_limosus
					                Klebsiella_oxytoca
					                Klebsiella_pneumoniae
					                Klebsiella_quasipneumoniae
					                Klebsiella_quasivariicola
					                Klebsiella_variicola
					                Neisseria_meningitidis
					                Pseudomonas_aeruginosa
					                Salmonella_enterica
					                Serratia_marcescens
					                Staphylococcus_aureus
					                Staphylococcus_epidermidis

				* Example run.  Make sure to put results in results folder

					$ python micro_pipeline_v2.py -d ../URMC_micro_project_data/samples_not_assigned_projects/example_project_folders/E_col/ -species Escherichia_coli -o ../results/example_E_col_step1		

				* check that is running (replace my user name with yours)

					$ squeue -u staffner

			* Run micro_pipeline_SNP_Calling_v2.py.py

				* show help menu

					$ python micro_pipeline_SNP_Calling_v2.py -h

				* In this example I'm only going to run E_col project folder.  The first step is to find substrain

					$ python micro_pipeline_SNP_Calling_v2.py.py -refs				
					....
					############################################
					Escherichia_coli

					-----------
					Escherichia_coli_AZ146

					-substrain /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_AZ146/Escherichia_coli_AZ146

					-----------
					Escherichia_coli_266917_2

					-substrain /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_266917_2/Escherichia_coli_266917_2

					-----------
					Escherichia_coli_656

					-substrain /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_656/Escherichia_coli_656

					-----------
					Escherichia_coli_MG1655

					-substrain /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_MG1655/Escherichia_coli_MG1655
					...

				* Refs will print out all of the references installed.  They will be separated by species.  There are 4 installed Ecoli references.  I will choose to use the Escherichia_coli_MG1655 reference.  Therefore I will copy the entire address.

					-substrain /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_MG1655/Escherichia_coli_MG1655

				* Run
					
					$ python micro_pipeline_SNP_Calling_v2.py -d ../URMC_micro_project_data/samples_not_assigned_projects/example_project_folders/E_col/ -o ../results/example_E_col -substrain /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_MG1655/Escherichia_coli_MG1655

				* check that is running (replace my user name with yours)

					$ squeue -u staffner

# put_fastq_in_folders.py for Novogene

	* put_fastq_in_folders.py has 3 options that are used with novogene sequenced samples.  This is because the files are a little different for novogene 

		-seq_loc
		-species
		-md5_file

	* **Make sure all samples have a unique name**.  This includes previously sequenced files from Novogene.  I didn't add an automated version to rename samples because I didn't run into this problem but I assume Novogene might name the samples the same each time.  If you rename manually before running put_fastq_in_folders.py the md5 check will not work.  Therefore renaming manually after running put_fastq_in_folders.py would be best. 

	* This script is expecting novogene to follow the same naming convention that I saw before.  If this isn't true then this script will not work.  The naming convention is Name_readnum.fq.gz

	* Example novogene data is at 

		Box\micro\about_micro_pipeline\pipeline_backup\samanthas_last_pipeline_backup\example_data_and_run_files\novogene

	* md5_file is provided by novogene and ensures files are copied in full.  

	* species this will change the novogene name to include the species.  Make sure you follow the recommened naming and include capital genus underscore lower case 3 letters for species.  For instance S_aur or E_col.  If there is a mix of species in the folder separate the files into species folders and then run them in species group folders.

	* I transfered the example data to bluehive for this example.

		$ pwd
		/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/project_data
		$ ls
		lib  MD5.txt  novogene_raw_reads  put_fastq_in_folders.py  test_install_data
		$ ls -1 novogene_raw_reads/
		IHMA_1_1.fq.gz
		IHMA_1_2.fq.gz
		IHMA_2_1.fq.gz
		IHMA_2_2.fq.gz
		IHMA_3_1.fq.gz
		IHMA_3_2.fq.gz

	* run put_fastq_in_folders.py

		$ python put_fastq_in_folders.py -d novogene_raw_reads/ -md5_file MD5.txt -species S_aur -seq_loc novogene
		{'IHMA_3_1.fq.gz': '1a4bc3776345b90ea4840a02107ae9db', 'IHMA_2_2.fq.gz': 'cdf95bd0c609baf98ed76eda83f03a2a', 'IHMA_1_2.fq.gz': 'fb8e78c49153154379b3e74e4172479b', 'IHMA_3_2.fq.gz': '7f4038bef542757a63296c89e5353c13', 'IHMA_2_1.fq.gz': '32217e627a03ed1629015110944cd7f4', 'IHMA_1_1.fq.gz': 'd1d8b527c5c8f32d1e92894c1df70155'}
		Current md5 file check IHMA_1_1.fq.gz
		        curr_file_hash = d1d8b527c5c8f32d1e92894c1df70155
		        expected_hash = d1d8b527c5c8f32d1e92894c1df70155
		        Do they Match? True
		Current md5 file check IHMA_2_2.fq.gz
		        curr_file_hash = cdf95bd0c609baf98ed76eda83f03a2a
		        expected_hash = cdf95bd0c609baf98ed76eda83f03a2a
		        Do they Match? True
		Current md5 file check IHMA_3_2.fq.gz
		        curr_file_hash = 7f4038bef542757a63296c89e5353c13
		        expected_hash = 7f4038bef542757a63296c89e5353c13
		        Do they Match? True
		Current md5 file check IHMA_3_1.fq.gz
		        curr_file_hash = 1a4bc3776345b90ea4840a02107ae9db
		        expected_hash = 1a4bc3776345b90ea4840a02107ae9db
		        Do they Match? True
		Current md5 file check IHMA_2_1.fq.gz
		        curr_file_hash = 32217e627a03ed1629015110944cd7f4
		        expected_hash = 32217e627a03ed1629015110944cd7f4
		        Do they Match? True
		Current md5 file check IHMA_1_2.fq.gz
		        curr_file_hash = fb8e78c49153154379b3e74e4172479b
		        expected_hash = fb8e78c49153154379b3e74e4172479b
		        Do they Match? True

		* The script is checking that the file is exactly the same as what Novogene said the gene was with md5 hash.  If they are the same it says True.  Then it renames the file to include S_aur.

	*  This is what the newly named samples look like.

		$ ls -1 novogene_raw_reads/
		IHMA_1_S_aur
		IHMA_2_S_aur
		IHMA_3_S_aur
		$ ls -1 novogene_raw_reads/IHMA_1_S_aur/
		IHMA_1_S_aur_R1.fastq.gz
		IHMA_1_S_aur_R2.fastq.gz

	* Now manually move them all into a species project folder if you didn't already do this

# Make a csv or text file as input

	* Make sure to make the file in a text editor like sublime.  **DO NOT MAKE IN EXCEL**
	* Set line endings to UNIX using sublime

		View -> Line Endings -> UNIX

	* Do not leave an empty line

# Contig based analysis
	* Purpose: Run Blast, MLST typing, Prokka, and parsnp on a folder of contigs files.  
	* Place one contig file for each sample in a folder
	* Example files are at 

		Box\micro\about_micro_pipeline\pipeline_backup\samanthas_last_pipeline_backup\example_data_and_run_files\E_clo_refseq_contigs

	* I transfered the example data set to bluehive

		$ pwd
		/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/project_data/E_clo_refseq_contigs

	$ run micro_pipeline_folder_contigs.py
	
		* show help menu
			
			$ python micro_pipeline_folder_contigs.py -h

		* find installed species

			$ python micro_pipeline_folder_contigs.py -refs

		* find installed substrains

			$ python micro_pipeline_folder_contigs.py -refs_substrain			

		* run
			$ python micro_pipeline_folder_contigs.py -d ../project_data/E_clo_refseq_contigs/ -substrain /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/Enterobacter_cloacae/Enterobacter_cloacae_CAV1669/Enterobacter_cloacae_CAV1669 -o ../results/test_folder_contigs -species Enterobacter_cloacae

			* It will check all the contig files so for this example it will take a few minutes because there are so many input files

# Hybrid assembly

	* Add how to transfer files from the mac to bluehive to run guppy to call reads
		* Albacore is no longer supported.  Guppy replaced albcore.  Not sure when this happened but when we updated to the latest version of MinKnow (I think it should be MinKnow  4.0.20) Albacore no longer worked. 2020-08-31
	
		* Transfer all files to box using box drive on the Nanopore Mac Computer
		
			* Login to Box drive.
			* It will appear as another drive in your finder 
			* copy all files to 
				*/micro/minion/raw_data

		* Transfer to bluehive using rclone 
	  		* Example sbatch file can be found at Box\micro\about_micro_pipeline\pipeline_backup\samanthas_last_pipeline_backup\example_data_and_run_files\rclone_minion.sh
	  		* Copy the rclone file, then edit the rclone portion of this file, and transfer it to bluehive
	  		* find the file via the commandline on bluehive and execute the file by running the following command

	  			$ sbatch rclone_minion_copy.sh

	* load guppy

		$ module load guppy/4.0.15  

	* Show guppy_basecaller help menu to decide how to setup the sbatch file to run guppy.  Make sure to run guppy via a sbatch file since it takes a long time to run and you do not want service interruption with bluehive to stop guppy.

		$ guppy_basecaller -h

		 Guppy Basecalling Software, (C) Oxford Nanopore Technologies, Limited. Version 4.0.15+5694074, client-server API version 2.1.0

		Usage:

		With config file:"
		  guppy_basecaller -i <input path> -s <save path> -c <config file> [options]
		With flowcell and kit name:
		  guppy_basecaller -i <input path> -s <save path> --flowcell <flowcell name>
		    --kit <kit name>
		List supported flowcells and kits:
		  guppy_basecaller --print_workflows

		Use server for basecalling:
		  guppy_basecaller -i <input path> -s <save path> -c <config file>
		    --port <server address> [options]
		Command line parameters:
		  --trim_threshold arg              Threshold above which data will be trimmed
		                                    (in standard deviations of current level
		                                    distribution).
		  --trim_min_events arg             Adapter trimmer minimum stride intervals
		                                    after stall that must be seen.
		  --max_search_len arg              Maximum number of samples to search through
		                                    for the stall
		  --override_scaling                Manually provide scaling parameters rather
		                                    than estimating them from each read.
		  --scaling_med arg                 Median current value to use for manual
		                                    scaling.
		  --scaling_mad arg                 Median absolute deviation to use for manual
		                                    scaling.
		  --trim_strategy arg               Trimming strategy to apply: 'dna' or 'rna'
		                                    (or 'none' to disable trimming)
		  --dmean_win_size arg              Window size for coarse stall event
		                                    detection
		  --dmean_threshold arg             Threshold for coarse stall event detection
		  --jump_threshold arg              Threshold level for rna stall detection
		  --pt_scaling                      Enable polyT/adapter max detection for read
		                                    scaling.
		  --pt_median_offset arg            Set polyT median offset for setting read
		                                    scaling median (default 2.5)
		  --adapter_pt_range_scale arg      Set polyT/adapter range scale for setting
		                                    read scaling median absolute deviation
		                                    (default 5.2)
		  --pt_required_adapter_drop arg    Set minimum required current drop from
		                                    adapter max to polyT detection. (default
		                                    30.0)
		  --pt_minimum_read_start_index arg Set minimum index for read start sample
		                                    required to attempt polyT scaling. (default
		                                    30)
		  --as_model_file arg               Path to JSON model file for adapter
		                                    scaling.
		  --as_gpu_runners_per_device arg   Number of runners per GPU device for
		                                    adapter scaling.
		  --as_cpu_threads_per_scaler arg   Number of CPU worker threads per adapter
		                                    scaler
		  --as_reads_per_runner arg         Maximum reads per runner for adapter
		                                    scaling.
		  --as_num_scalers arg              Number of parallel scalers for adapter
		                                    scaling.
		  -m [ --model_file ] arg           Path to JSON model file.
		  -k [ --kernel_path ] arg          Path to GPU kernel files location (only
		                                    needed if builtin_scripts is false).
		  -x [ --device ] arg               Specify basecalling device: 'auto', or
		                                    'cuda:<device_id>'.
		  --builtin_scripts arg             Whether to use GPU kernels that were
		                                    included at compile-time.
		  --chunk_size arg                  Stride intervals per chunk.
		  --chunks_per_runner arg           Maximum chunks per runner.
		  --chunks_per_caller arg           Soft limit on number of chunks in each
		                                    caller's queue. New reads will not be
		                                    queued while this is exceeded.
		  --high_priority_threshold arg     Number of high priority chunks to process
		                                    for each medium priority chunk.
		  --medium_priority_threshold arg   Number of medium priority chunks to process
		                                    for each low priority chunk.
		  --overlap arg                     Overlap between chunks (in stride
		                                    intervals).
		  --gpu_runners_per_device arg      Number of runners per GPU device.
		  --cpu_threads_per_caller arg      Number of CPU worker threads per
		                                    basecaller.
		  --num_callers arg                 Number of parallel basecallers to create.
		  --post_out                        Return full posterior matrix in output
		                                    fast5 file and/or called read message from
		                                    server.
		  --stay_penalty arg                Scaling factor to apply to stay probability
		                                    calculation during transducer decode.
		  --qscore_offset arg               Qscore calibration offset.
		  --qscore_scale arg                Qscore calibration scale factor.
		  --temp_weight arg                 Temperature adjustment for weight matrix in
		                                    softmax layer of RNN.
		  --temp_bias arg                   Temperature adjustment for bias vector in
		                                    softmax layer of RNN.
		  --qscore_filtering                Enable filtering of reads into PASS/FAIL
		                                    folders based on min qscore.
		  --min_qscore arg                  Minimum acceptable qscore for a read to be
		                                    filtered into the PASS folder
		  --reverse_sequence arg            Reverse the called sequence (for RNA
		                                    sequencing).
		  --u_substitution arg              Substitute 'U' for 'T' in the called
		                                    sequence (for RNA sequencing).
		  --log_speed_frequency arg         How often to print out basecalling speed.
		  --barcode_kits arg                Space separated list of barcoding kit(s) or
		                                    expansion kit(s) to detect against. Must be
		                                    in double quotes.
		  --trim_barcodes                   Trim the barcodes from the output sequences
		                                    in the FastQ files.
		  --num_extra_bases_trim arg        How vigorous to be in trimming the barcode.
		                                    Default is 0 i.e. the length of the
		                                    detected barcode. A positive integer means
		                                    extra bases will be trimmed, a negative
		                                    number is how many fewer bases (less
		                                    vigorous) will be trimmed.
		  --arrangements_files arg          Files containing arrangements.
		  --score_matrix_filename arg       File containing mismatch score matrix.
		  --start_gap1 arg                  Gap penalty for aligning before the
		                                    reference.
		  --end_gap1 arg                    Gap penalty for aligning after the
		                                    reference.
		  --open_gap1 arg                   Penalty for opening a new gap in the
		                                    reference.
		  --extend_gap1 arg                 Penalty for extending a gap in the
		                                    reference.
		  --start_gap2 arg                  Gap penalty for aligning before the query.
		  --end_gap2 arg                    Gap penalty for aligning after the query.
		  --open_gap2 arg                   Penalty for opening a new gap in the query.
		  --extend_gap2 arg                 Penalty for extending a gap in the query.
		  --min_score arg                   Minimum score to consider a valid
		                                    alignment.
		  --min_score_rear_override arg     Minimum score to consider a valid alignment
		                                    for the rear barcode only (and min_score
		                                    will then be used for the front only when
		                                    this is set).
		  --front_window_size arg           Window size for the beginning barcode.
		  --rear_window_size arg            Window size for the ending barcode.
		  --require_barcodes_both_ends      Reads will only be classified if there is a
		                                    barcode above the min_score at both ends of
		                                    the read.
		  --allow_inferior_barcodes         Reads will still be classified even if both
		                                    the barcodes at the front and rear (if
		                                    applicable) were not the best scoring
		                                    barcodes above the min_score.
		  --detect_mid_strand_barcodes      Search for barcodes through the entire
		                                    length of the read.
		  --min_score_mid_barcodes arg      Minimum score for a barcode to be detected
		                                    in the middle of a read.
		  --num_barcoding_buffers arg       Number of GPU memory buffers to allocate to
		                                    perform barcoding into. Controls level of
		                                    parallelism on GPU for barcoding.
		  --num_barcode_threads arg         Number of worker threads to use for
		                                    barcoding.
		  --calib_detect                    Enable calibration strand detection and
		                                    filtering.
		  --calib_reference arg             Reference FASTA file containing calibration
		                                    strand.
		  --calib_min_sequence_length arg   Minimum sequence length for reads to be
		                                    considered candidate calibration strands.
		  --calib_max_sequence_length arg   Maximum sequence length for reads to be
		                                    considered candidate calibration strands.
		  --calib_min_coverage arg          Minimum reference coverage to pass
		                                    calibration strand detection.
		  --print_workflows                 Output available workflows.
		  --flowcell arg                    Flowcell to find a configuration for
		  --kit arg                         Kit to find a configuration for
		  -a [ --align_ref ] arg            Path to alignment reference.
		  --bed_file arg                    Path to .bed file containing areas of
		                                    interest in reference genome.
		  --num_alignment_threads arg       Number of worker threads to use for
		                                    alignment.
		  -z [ --quiet ]                    Quiet mode. Nothing will be output to
		                                    STDOUT if this option is set.
		  --trace_categories_logs arg       Enable trace logs - list of strings with
		                                    the desired names.
		  --verbose_logs                    Enable verbose logs.
		  --trace_domains_log arg           List of trace domains to include in verbose
		                                    logging (if enabled),  '*' for all.
		  --trace_domains_config arg        Configuration file containing list of trace
		                                    domains to include in verbose logging (if
		                                    enabled), this will override
		                                    --trace_domain_logs
		  --disable_pings                   Disable the transmission of telemetry
		                                    pings.
		  --ping_url arg                    URL to send pings to
		  --ping_segment_duration arg       Duration in minutes of each ping segment.
		  --progress_stats_frequency arg    Frequency in seconds in which to report
		                                    progress statistics, if supplied will
		                                    replace the default progress display.
		  -q [ --records_per_fastq ] arg    Maximum number of records per fastq file, 0
		                                    means use a single file (per worker, per
		                                    run id).
		  --read_batch_size arg             Maximum batch size, in reads, for grouping
		                                    input files.
		  --compress_fastq                  Compress fastq output files with gzip.
		  -i [ --input_path ] arg           Path to input fast5 files.
		  --input_file_list arg             Optional file containing list of input
		                                    fast5 files to process from the input_path.
		  -s [ --save_path ] arg            Path to save fastq files.
		  -l [ --read_id_list ] arg         File containing list of read ids to filter
		                                    to
		  -r [ --recursive ]                Search for input files recursively.
		  --fast5_out                       Choice of whether to do fast5 output.
		  --resume                          Resume a previous basecall run using the
		                                    same output folder.
		  --max_block_size arg              Maximum block size (in events) of basecall
		                                    messages to server.
		  -p [ --port ] arg                 Port for basecalling service.
		  --barcoding_config_file arg       Configuration file to use for barcoding.
		  --disable_events                  Disable the transmission of event tables
		                                    when receiving reads back from the basecall
		                                    server.
		  --client_id arg                   Optional unique identifier (non-negative
		                                    integer) for this instance of the Guppy
		                                    Client Basecaller, if supplied will form
		                                    part of the output filenames.
		  --nested_output_folder            If flagged output fastq files will be
		                                    written to a nested folder structure, based
		                                    on: protocol_group/sample/protocol/qscore_p
		                                    ass_fail/barcode_arrangement/
		  --server_file_load_timeout arg    Timeout in seconds to wait for the server
		                                    to load a requested data file (e.g. a
		                                    basecalling model or alignment index).
		                                    default is 30s
		  --max_queued_reads arg            Maximum number of reads to be submitted for
		                                    processing at any one time.
		  -h [ --help ]                     produce help message
		  -v [ --version ]                  print version number
		  -c [ --config ] arg               Config file to use
		  -d [ --data_path ] arg            Path to use for loading any data files the
		                                    application requires.

	* run guppy via an sbatch file.  
		* an example sbatch file can be found at Box\micro\about_micro_pipeline\pipeline_backup\samanthas_last_pipeline_backup\example_data_and_run_files\guppy_sbatch.sh
	  		* Copy the guppy_sbatch file, then edit the guppy_basecaller portion of this file, and transfer it to bluehive
	  		* find the file via the commandline on bluehive and execute the file by running the following command

	  			$ sbatch guppy_basecaller.sh

	* Run micro_pipeline_nanopore.py

		* Example dataset is at Box\micro\about_micro_pipeline\pipeline_backup\samanthas_last_pipeline_backup\example_data_and_run_files\minion

		* I transfered the example dataset to bluehive

			$ pwd
			/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/project_data/minion
			$ ls -1
			miseq_dir
			nanopore_dir
			sample_link_file.csv

		* sample_link_file.csv

			* Make sure to make this file in a text editor like sublime.  **DO NOT MAKE IN EXCEL**
			* Set line endings to UNIX using sublime

				View -> Line Endings -> UNIX

			* Do not leave an empty line

		* move to the micro_pipeline_v2 directory

			$ cd ../../micro_pipeline_v2
			$ pwd
			/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2

		* show help menu and then run micro_pipeline_nanopore.py

			$ python micro_pipeline_nanopore.py -h
			$ python micro_pipeline_nanopore.py -nanopore_dir ../project_data/minion/nanopore_dir/ -miseq_dir ../project_data/minion/miseq_dir/ -sample_link_file ../project_data/minion/sample_link_file.csv -o ../results/test_hybrid


		$ python micro_pipeline_nanopore.py -nanopore_dir ../project_data/minion/nanopore_dir/ -miseq_dir ../project_data/minion/miseq_dir/ -sample_link_file ../project_data/minion/sample_link_file.csv -o ../results/test_hybrid
		/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2/lib
		/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/db/micro_pipeline.db
		URMC_638_E_col,barcode01
		URMC_639_E_col,barcode02
		URMC_640_E_col,barcode03
		found rev file ../project_data/minion/miseq_dir/URMC_638_E_col/URMC_638_E_col_R2_001hg19_filtered_R2_trimmed.fq.gz
		found for file ../project_data/minion/miseq_dir/URMC_638_E_col/URMC_638_E_col_R1_001hg19_filtered_R1_trimmed.fq.gz

		###############################
		MICRO PIPELINE UPDATE:
		        2021_07_30 13:28:20
		        Forward and reverse human removed trimmed reads found.  Skipping these steps.
		###############################


		Submitted batch job 5920628

		Sample URMC_638_E_col_barcode01 was submitted under job number 5920628
		found for file ../project_data/minion/miseq_dir/URMC_639_E_col/URMC_639_E_col_R1_001hg19_filtered_R1_trimmed.fq.gz
		found rev file ../project_data/minion/miseq_dir/URMC_639_E_col/URMC_639_E_col_R2_001hg19_filtered_R2_trimmed.fq.gz

		###############################
		MICRO PIPELINE UPDATE:
		        2021_07_30 13:28:22
		        Forward and reverse human removed trimmed reads found.  Skipping these steps.
		###############################


		Submitted batch job 5920629

		Sample URMC_639_E_col_barcode02 was submitted under job number 5920629
		found rev file ../project_data/minion/miseq_dir/URMC_640_E_col/URMC_640_E_col_R2_001hg19_filtered_R2_trimmed.fq.gz
		found for file ../project_data/minion/miseq_dir/URMC_640_E_col/URMC_640_E_col_R1_001hg19_filtered_R1_trimmed.fq.gz

		###############################
		MICRO PIPELINE UPDATE:
		        2021_07_30 13:28:22
		        Forward and reverse human removed trimmed reads found.  Skipping these steps.
		###############################


		Submitted batch job 5920630

		Sample URMC_640_E_col_barcode03 was submitted under job number 5920630

	* check to see that it is running.  The jobs have not started yet.  Check back to see time turn from 0:00 to  a number counting up.

		$ squeue -u staffner
	             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
	           5920630  standard URMC_640 staffner PD       0:00      1 (Priority)
	           5920629  standard URMC_639 staffner PD       0:00      1 (Priority)
	           5920628  standard URMC_638 staffner PD       0:00      1 (Priority)

# Pipeline Info
	* Store all analysis output under -o in the /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/results folder

	* Database

		* The pipeline uses a SQLite database to store QC and MSLT data for reanalysis.  
		* The database is located at

			/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/db/micro_pipeline.db	

		* NEVER DELETE THIS file

	* References, Blast databases, and MLST schemes need to be added correctly to the /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences folder.  Make sure you follow the instructions below.

	* All python scripts mentioned have detailed help menus that can be accessed using the -h tag.  Example use would be:

		$ cd /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2
		$ micro_pipeline_v2.py -h

	* To run any of these programs navigate to /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2

		$ cd /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2

	* species input
		* required for micro_pipeline_folder_contigs and micro_pipeline_v2
		* Use the -refs flag to list all species references installed

			$ python micro_pipeline_v2.py -refs
  	
		* The above command will list all installed species.
		* Make sure to copy the entire species name to avoid typos Example would be:

			-species Clostridium_difficile

  	* substrain
  		* required to make trees 
  		* Make sure to use -refs flag in micro_pipeline_SNP_calling_v2.py or -refs_substrain flag while using the micro_pipline_folder_contigs.py script to find installed references.

  			$ python micro_pipeline_folder_contigs.py -refs_substrain

        * The refs or refs_sustrain flag will print out all installed references separated by species.
        * Scroll to find the reference you want to use.
        * Make sure to copy the entire address for example use the following -substrain input to run a Clostridium_difficile using reference Clostridium_difficile_R20291

        	-substrain /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/Clostridium_difficile/Clostridium_difficile_R20291/Clostridium_difficile_R20291

# backup bluehive

	* I back up my scratch drive on bluehive once a month to box using rclone.

# put_fastq_in_folders.py help menu

	[staffner@bluehive samples_not_assigned_projects]$ python put_fastq_in_folders.py -h
	usage: put_fastq_in_folders.py [-h] [-d [D]] [-seq_loc [SEQ_LOC]]
	                               [-species [SPECIES]] [-md5_file [MD5_FILE]]

	-----------------------------------------------------------------------------------

	Run this program first to put your fastq files in a folder.

	This program I thru together quickly and it requires the folder containing all fastq files to be in the same folder that put_fastq_in_folders.py

	-----------------------------------------------------------------------------------

	optional arguments:
	  -h, --help            show this help message and exit
	  -d [D]                <dir> directory containing all fastq files
	  -seq_loc [SEQ_LOC]    <str> Optional. Choose location that sequencing was
	                        performed. Since different places name files
	                        differently we will need to make sure everything is
	                        formated correctly for micro pipeline. Options are
	                        novogene or urmc_molecular. Default is urmc_molecular.
	                        To select novogene use -seq_loc novogene
	  -species [SPECIES]    <str> (ONLY Required if your sequence was sequenced at
	                        NOVOGENE.) To add a species to all of the samples
	                        supply an abbreviation. For instance S_aur. -species
	                        only works for -seq_loc novogene.
	  -md5_file [MD5_FILE]  <file> Supply a md5 file where the first column is the
	                        md5 of original file. Second column file name. If the
	                        current file does not match the md5 in the original
	                        file the files will not be renamed and they will not
	                        put in folders and an error will be presented.
	                        -md5_file only works for -seq_loc novogene.

	-----------------------------------------------------------------------------------
	URMC MiSeq DATA Examples

	Example -d folder:
	        d_folder
	              URMC-95-E-col_S14_L001_R2_001.fastq.gz
	              URMC-95-E-col_S14_L001_R1_001.fastq.gz

	Example out put (use this folder for input into micro_pipeline_v2.py)
	        d_folder
	                     URMC_95_E_col
	                  URMC_95_E_col_S14_L001_R2_001.fastq.gz
	                  URMC_95_E_col_S14_L001_R1_001.fastq.gz

	Example run for sequences run at URMC MiSeq:

	        Navigate to URMC_micro_project_data/samples_not_assigned_projects folder:
	              python put_fastq_in_folders.py -d Pecora_02_22_18/

	-----------------------------------------------------------------------------------
	NOVOGENE DATA Examples

	Example -d folder:
	        d_folder
	                        RawData_MD5.txt
	              IHMA_83_1.fq.gz
	              IHMA_83_2.fq.gz

	Example out put (use this folder for input into micro_pipeline_v2.py)
	        d_folder
	                     IHMA_83_S_aur
	                  IHMA_83_S_aur_R1.fastq.gz
	                  IHMA_83_S_aur_R1.fastq.gz

	Example run for sequences run at novogene:

	        Navigate to URMC_micro_project_data/samples_not_assigned_projects folder:
	              python put_fastq_in_folders.py -d NovoGene_Wozniak_2019_08_28_before_rename/ -md5_file /Wozniak_2020_06_03/MD5.txt -seq_loc novogene -species S_aur              

# micro_pipeline_v2.py help menu

	$ python micro_pipeline_v2.py -h
	/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2/lib
	/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/db/micro_pipeline.db
	usage: micro_pipeline_v2.py [-h] [--version] [-refs] [-d [INPUT_DIR]]
	                            [-contig_dir [CONTIG_DIR]] [-species [SPECIES]]
	                            [-o [O]] [-trim_window_size [TRIM_WINDOW_SIZE]]
	                            [-trim_quality [TRIM_QUALITY]]
	                            [-trim_min_len [TRIM_MIN_LEN]]
	                            [-blast_cut_off [BLAST_CUT_OFF]]
	                            [-len_contig_cutoff [LEN_CONTIG_CUTOFF]]
	                            [-coverage_min [COVERAGE_MIN]]
	                            [-extra_cols [EXTRA_COLS]]
	                            [-partition [PARTITION]] [-rm_human_skip]
	                            [-trim_skip] [-mapping_qc_skip] [-spades_skip]
	                            [-force_retrim_spades] [-force_spades]
	                            [-contig_qc_skip] [-strainseeker_skip]
	                            [-plasmid_contig_skip] [-blast_db_skip]
	                            [-plasmid_blast_db_skip] [-quast_skip]
	                            [-skip_prokka] [-MLST_skip]
	                            [-do_not_delete_intermediate]

	-----------------------------------------------------------------------------------

	Welcome to the Microbiology Pipeline

	-----------------------------------------------------------------------------------
	The purpose of this is to run a consistent pipeline for analysis of bacterial isolates on a Slurm Linux Cluster from paired end raw reads

	micro_pipeline_v2.py uses the following software, languages, database
	        python2, biopython v1.70, java, perl, R, sqlite, bowtie2 v2.2.9, samtools v1.5, trimmomatic v0.36, fastqc 0.11.5, picard v2.12.0, spades v3.11.1, quast v4.5, strainseeker v1.5, blast, prokka v1.12

	-----------------------------------------------------------------------------------

	optional arguments:
	  -h, --help            show this help message and exit
	  --version             show program's version number and exit
	  -refs                 <flag> Use only this flag if you would like all
	                        possible references printed.
	  -d [INPUT_DIR], --input_dir [INPUT_DIR]
	                        <directory> If multiple samples need to be analyzed a
	                        directory can be included. This directory needs a
	                        specific format. Refer to the bottom of the page for
	                        more information. Turns off the need for -1 and -2
	                        flags.
	  -contig_dir [CONTIG_DIR]
	                        <directory> If multiple contig files need to be
	                        analyzed a directory of contigs can be included. Turns
	                        off the need for -1, -2, or -contig flags.
	  -species [SPECIES]    <str> Include species a species name. The species is
	                        required to be included in possible references already
	                        included. Use -refs flag to list possible refs. Make
	                        sure the space is replaced with _ example
	                        Escherichia_coli.
	  -o [O]                <dir> Directory to save output. Default:
	                        micro_pipeline_output_run_id
	  -trim_window_size [TRIM_WINDOW_SIZE]
	                        <int> Set trimmomatic window size. Specifies the
	                        number of bases to average across. Default is 4
	  -trim_quality [TRIM_QUALITY]
	                        <int> Set trimmomatic sliding window required quality.
	                        Specifies the average quality required. Default is 20
	  -trim_min_len [TRIM_MIN_LEN]
	                        <int> Set trimmomatic minimal length. Specifies the
	                        minimum length of reads to be kept. Default is 100
	  -blast_cut_off [BLAST_CUT_OFF]
	                        <int> Set the blast cut off. Default is 90
	  -len_contig_cutoff [LEN_CONTIG_CUTOFF]
	                        <int> he pipeline will produce a filtered contig file
	                        using both len_contig_cutoff and coverage_min. Prokka
	                        annotation will use this contig file. These values are
	                        reported by the spades assembler and are related to
	                        the assembly. All contigs with an assembly coverage
	                        smaller than coverage_min will be removed from the
	                        file. Default is 10.
	  -coverage_min [COVERAGE_MIN]
	                        <int> The pipeline will produce a filtered contig file
	                        using both len_contig_cutoff and coverage_min. Prokka
	                        annotation will use this contig file. These values are
	                        reported by the spades assembler and are related to
	                        the assembly. All contigs with an assembly coverage
	                        smaller than coverage_min will be removed from the
	                        file. Default is 10.
	  -extra_cols [EXTRA_COLS]
	                        <file> This tag is used to add extra identifiers
	                        columns to matrix. For example add a location column.
	                        To use this tag provide the full address to a csv file
	                        where the first column the sample name followed by any
	                        number of columns are the columns to insert. The first
	                        row needs to be the names of the columns. Do not had
	                        _contigs.fasta to file name.
	  -partition [PARTITION]
	                        <str> Add a run partition to sbatch script.
	  -rm_human_skip        <flag> Use this flag if you would like to skip the
	                        step to remove human reads.
	  -trim_skip            <flag> Use this flag to skip trimming.
	  -mapping_qc_skip      <flag> Use this flag to skip fastqc.
	  -spades_skip          <flag> Use this flag to skip SPAdes assembly.
	  -force_retrim_spades  <flag> Use this flag force the retrimming and
	                        reassembly of a contig file. Normally if these steps
	                        have already been completed on a sample SPAdes is
	                        skipped automatically.
	  -force_spades         <flag> Use this flag force the reassembly of a contig
	                        file. Normally if this step has already been completed
	                        on a sample SPAdes is skipped automatically.
	  -contig_qc_skip       <flag> Use this flag to skip qc of contigs. contig qc
	                        will always be skipped if a new contig file was not
	                        made however, contig qc will be included in report.
	                        contig qc is performed by quast.
	  -strainseeker_skip    <flag> Use this flag to skip strainseeker.
	  -plasmid_contig_skip  <flag> Use this flag to skip assembling plasmids from
	                        contigs. This step is automatically skipped if file is
	                        found in input folder.
	  -blast_db_skip        <flag> Use this flag to skip finding all matches in
	                        blast dbs.
	  -plasmid_blast_db_skip
	                        <flag> Use this flag to skip finding all matches
	                        between plasmid contigs and plasmid blast dbs.
	  -quast_skip           <flag> Use this flag to skip genomic fractions with
	                        quast.
	  -skip_prokka          <flag> Use this flag to skip annotation of contig with
	                        prokka.
	  -MLST_skip            <flag> Use this flag to skip MLST typing.
	  -do_not_delete_intermediate
	                        <flag> Use this flag so the intermediate processing
	                        files are not deleted.

	-----------------------------------------------------------------------------------
	Example run:

	-----------------------------------------------------------------------------------
	        STEP 1 (List all reference species):

	        Navigate to micro_pipeline_v2 folder:
	                        $ python micro_pipeline_v2.py -refs

	Current species with references are:
	     Citrobacter_freundii
	     Clostridium_difficile
	     Corynebacterium_diphtheriae
	     Corynebacterium_ulcerans
	     Enterobacter_aerogenes
	     Enterobacter_cloacae
	     Escherichia_coli
	     Facklamia_hominis
	     Human_coxsackievirus
	     Inquilinus_limosus
	     Klebsiella_oxytoca
	     Klebsiella_pneumoniae
	     Neisseria_meningitidis
	     Salmonella_enterica
	     Serratia_marcescens
	     Staphylococcus_aureus

	Copy the species that you suspect that you strain is.  It is required to provide a suspected reference species.  If your species is not installed notify Samantha to install it.  If you do not know the species just choose a random one and the pipeline will use strainseeker to find out what the species report it in the run_stats file.

	-----------------------------------------------------------------------------------
	        STEP 2 (Run Pipeline):

	        Navigate to micro_pipeline_v2 folder:
	             $ python micro_pipeline_v2.py -species Escherichia_coli -d ../raw_reads/Pecora_02_22_18/E_coli -o ../Pecora_02_22_18_E_coli

# micro_pipeline_SNP_Calling_v2.py help menu
	$ python micro_pipeline_SNP_Calling_v2.py -h
	usage: micro_pipeline_SNP_Calling_v2.py [-h] [--version] [-refs] [-ref_info]
	                                        [-d [INPUT_DIR]]
	                                        [-substrain [SUBSTRAIN]] [-o [O]]
	                                        [-window_size [WINDOW_SIZE]]
	                                        [-max_num_snps [MAX_NUM_SNPS]]
	                                        [-ignore_samples [IGNORE_SAMPLES]]
	                                        [-sample_subset [SAMPLE_SUBSET]]
	                                        [-force_remap] [-filter_regions]
	                                        [-cov_snp_app] [-MLST_trees]
	                                        [-rm_phage] [-rm_nulls]
	                                        [-mapping_type [MAPPING_TYPE]]
	                                        [-vis_skip]

	-----------------------------------------------------------------------------------

	Welcome to the Microbiology SNP Calling Pipeline

	-----------------------------------------------------------------------------------
	The purpose of this is to run phylogenetic analysis on all samples in a directory
	-----------------------------------------------------------------------------------

	optional arguments:
	  -h, --help            show this help message and exit
	  --version             show program's version number and exit
	  -refs                 <flag> Use only this flag if you would like all
	                        possible references printed.
	  -ref_info             <flag> Use only this flag to describe more more
	                        information about the reference
	  -d [INPUT_DIR], --input_dir [INPUT_DIR]
	                        <directory> STEP MAP ONLY: All samples folders should
	                        be nested in the same folder. Submit the parent folder
	                        to this program. This directory needs a specific
	                        format. Refer to the bottom of the page for more
	                        information.
	  -substrain [SUBSTRAIN]
	                        <str> Include the substrain which would like to be
	                        used as the reference. The substrain is required to be
	                        included in possible references already included. Use
	                        -refs flag to list possible refs. Make sure the space
	                        is replaced with _ example Escherichia_coli_MG1655.
	                        Example: -substrain ../reference_sequences/Escherichia
	                        _coli/Escherichia_coli_MG1655/Escherichia_coli_MG1655
	  -o [O]                <dir> REQUIRED BOTH STEPS: Directory to save output.
	  -window_size [WINDOW_SIZE]
	                        <int> The length of the window in which the number of
	                        SNPs should be no more than max_num_snp. Default is 50
	  -max_num_snps [MAX_NUM_SNPS]
	                        <int> The maximum number of SNPs allowed in a window.
	                        Default is 3
	  -ignore_samples [IGNORE_SAMPLES]
	                        <file> Pass a file which is a list of sample names to
	                        ignore in analysis. Make sure the sample name is the
	                        exact same as the name of the folder that samples are
	                        saved in. This only works for the main tree. Default
	                        is off.
	  -sample_subset [SAMPLE_SUBSET]
	                        <file> Pass a file which is a list of sample names to
	                        run in the tree. This is only necessary if you would
	                        like to run a subset of samples. Make sure the sample
	                        name is the exact same as the name of the folder that
	                        samples are saved in. Each sample needs to be on a
	                        newline. Default is off.
	  -force_remap          <flag> Use this flag force the remapping of data for
	                        the input reference. Normally intermediate mapping
	                        files are saved for each reference the sample was
	                        mapped to in the sample folder.
	  -filter_regions       <flag> Turn off filtering by a window and number of
	                        snps in window. Use tags -window_size and
	                        -max_num_snps to change filtering criteria.
	  -cov_snp_app          <flag> Use this flag to turn on the SNP coverage app.
	                        This app will provide a local webpage to view the
	                        coverage of each SNP. This function will only run if
	                        you have less than 50 samples in your tree. Currently
	                        this option is set to not run automatically because
	                        there is a gut on line 1162 where the list index is
	                        out of range.
	  -MLST_trees           <flag> Turn off making MLST based trees. If left on it
	                        will make a tree for each individual ST type. Default
	                        is on.
	  -rm_phage             <flag> Turn on removing snps in phages, mobile
	                        elements, and transposons. Default is off.
	  -rm_nulls             <flag> Turn on removing nulls. Default is off.
	  -mapping_type [MAPPING_TYPE]
	                        <str> Use to choose type of mapping. Default is
	                        unpaired_bowtie2. Other options include
	                        paired_bowtie2.
	  -vis_skip             <flag> Use this flag to skip making visualization.
	                        THIS IS REQUIRED IF GENOME IS NOT FINISHED!

	-----------------------------------------------------------------------------------

	STEP 1 (List all reference substrains):

	        Navigate to micro_pipeline_v2 folder:
	                $ python micro_pipeline_SNP_calling_v2.py -refs

	Example output:
	############################################
	Escherichia_coli

	-----------
	Escherichia_coli_MG1655

	-substrain /scratch/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_MG1655/Escherichia_coli_MG1655

	-----------
	Escherichia_coli_AZ146

	-substrain /scratch/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_AZ146/Escherichia_coli_AZ146

	Choose a substrain reference to map all sequences to.  Make sure you copy the entire address and put it under the -substrain tag.

	-----------------------------------------------------------------------------------
	STEP 2 Run Pipeline:
	        Navigate to micro_pipeline_v2 folder:
	                $ python micro_pipeline_SNP_Calling_v2.py -o ../ecoli_tree -d ../original/ESBL_project/Excherichia_coli -substrain /scratch/staffner/test_micro_pipeline/reference_sequences/Escherichia_coli/Escherichia_coli_MG1655/Escherichia_coli_MG1655

# micro_pipeline_nanopore.py help menu
	$ python micro_pipeline_nanopore.py -h
	/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2/lib
	/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/db/micro_pipeline.db
	usage: micro_pipeline_nanopore.py [-h] [--version]
	                                  [-nanopore_dir [NANOPORE_DIR]]
	                                  [-miseq_dir [MISEQ_DIR]]
	                                  [-sample_link_file [SAMPLE_LINK_FILE]]
	                                  [-o [O]] [-run_one_blast_db]
	                                  [-nanoplot_skip]
	                                  [-blast_db_file [BLAST_DB_FILE]]

	-----------------------------------------------------------------------------------

	Welcome to the Microbiology Pipeline Hybrid Assembly

	-----------------------------------------------------------------------------------
	The purpose of this is to run a consistent pipeline for analysis of bacterial isolates on a Slurm Linux Cluster from paired end raw reads and nanopore long reads.

	-----------------------------------------------------------------------------------

	optional arguments:
	  -h, --help            show this help message and exit
	  --version             show program's version number and exit
	  -nanopore_dir [NANOPORE_DIR]
	                        <directory> Directory of all nanopore long reads after
	                        basecalling.
	  -miseq_dir [MISEQ_DIR]
	                        <directory> Directory containing sample directories of
	                        miseq paired end reads. For instance out_dir >
	                        sample_1 > for1.fq, rev1.fq
	  -sample_link_file [SAMPLE_LINK_FILE]
	                        <file> CSV file containing sample_id in first column
	                        and nanopore barcode in second column.
	  -o [O]                <dir> Directory to save output. Default:
	                        micro_pipeline_output
	  -run_one_blast_db     <flag> Turn on running a blast db with address to file
	                        under tag -blast_db_file. If turned on any contig with
	                        a hit under 60 60 will be placed in a hit folder in
	                        its on file. Default is off.
	  -nanoplot_skip        <flag> Use this flag to skip nanoplot.
	  -blast_db_file [BLAST_DB_FILE]
	                        <file> Address to blast db file to run. You can choose
	                        one database file to run.

	-----------------------------------------------------------------------------------
	Example run:

	python micro_pipeline_nanopore.py -nanopore_dir ../project_data/minion/nanopore_dir/ -miseq_dir ../project_data/minion/miseq_dir/ -sample_link_file ../project_data/minion/sample_link_file.csv -o ../results/test_hybrid

# micro_pipeline_folder_contigs.py help menu
	$ python micro_pipeline_folder_contigs.py -h
	/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2/lib
	/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/reference_sequences/db/micro_pipeline.db
	usage: micro_pipeline_folder_contigs.py [-h] [--version] [-d [INPUT_DIR]]
	                                        [-o [O]] [-skip_prokka] [-skip_blast]
	                                        [-skip_mlst] [-skip_parsnp]
	                                        [-strict_pident [STRICT_PIDENT]]
	                                        [-strict_pquery [STRICT_PQUERY]]
	                                        [-loose_pident [LOOSE_PIDENT]]
	                                        [-loose_pquery [LOOSE_PQUERY]] [-refs]
	                                        [-refs_substrain] [-species [SPECIES]]
	                                        [-substrain [SUBSTRAIN]]
	                                        [-extra_cols [EXTRA_COLS]]
	                                        [-blast_db [BLAST_DB]]

	-----------------------------------------------------------------------------------

	Run Blast, MLST typing, Prokka, and parsnp on a folder of contigs files.

	-----------------------------------------------------------------------------------
	Blast run criteria:  Each run has two criteria to run percent identity (pquery) and percent query (pquery).  The first condition is a default strict run.  Multi fasta is produced used strict conditions only.  The second condition is loose blast run conditions which let more hits in.

	A core genome parsnp tree will run using the reference provided under substrain

	MSLT based parsnp trees will automatically run if both blast and mlst is turned on

	optional arguments:
	  -h, --help            show this help message and exit
	  --version             show program's version number and exit
	  -d [INPUT_DIR], --input_dir [INPUT_DIR]
	                        <directory> A folder of fasta files.
	  -o [O]                <directory> output directory.
	  -skip_prokka          <flag> Turn off running prokka. Default is on.
	  -skip_blast           <flag> Turn off running blast. Default is on.
	  -skip_mlst            <flag> Turn off running MLST. However, skip_blast
	                        needs to be turned off also since MLST is required to
	                        make blast_summary file. Default is on. This will also
	                        turn off running MLST based parsnp trees.
	  -skip_parsnp          <flag> Turn off running running parsnp tree. To run
	                        parsnp a substrain is required.
	  -strict_pident [STRICT_PIDENT]
	                        <int> To change the loose pident settings enter an
	                        integer after this tag. Default is 90
	  -strict_pquery [STRICT_PQUERY]
	                        <int> To change the loose pquery settings enter an
	                        integer after this tag. Default is 90
	  -loose_pident [LOOSE_PIDENT]
	                        <int> To change the loose pident settings enter an
	                        integer after this tag. Default is 60
	  -loose_pquery [LOOSE_PQUERY]
	                        <int> To change the loose pquery settings enter an
	                        integer after this tag. Default is 60
	  -refs                 <flag> Use only this flag if you would like all
	                        possible species printed.
	  -refs_substrain       <flag> Use only this flag if you would like all
	                        possible substrains printed.
	  -species [SPECIES]    <str> species name required to run blast or MLST.
	  -substrain [SUBSTRAIN]
	                        <str> Include which substrain to use for the parsnp
	                        tree. Use -refs_substrain flag to list possible
	                        substrains. Example: -substrain /scratch/staffner/test
	                        _micro_pipeline/reference_sequences/Enterobacter_aerog
	                        enes/Enterobacter_aerogenes_KCTC_2190_uid66537/Enterob
	                        acter_aerogenes_KCTC_2190_uid66537
	  -extra_cols [EXTRA_COLS]
	                        <file> This tag is used to add extra identifiers
	                        columns to blast summaries matrix. For example add a
	                        location column. To use this tag provide the full
	                        address to a csv file where the first column the
	                        sample name followed by any number of columns are the
	                        columns to insert. The first row needs to be the names
	                        of the columns. Do not had _contigs.fasta to file
	                        name.
	  -blast_db [BLAST_DB]  <dir> To a supply a custom blast_db which is not
	                        installed use this option. The format is required to
	                        be one master folder with sub folders for each db
	                        type. Inside each blast db folder one or more .fsa
	                        files are required in multi fasta format

# Tips

	* Find a sample by using find to recursively search all directories 

		$ cd /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data
		$ find . -name "URMC_591*"

			* . means search current directory
			* * means the rest of the file name can be whatever

	* SLURM (replace staffner with your user name)

		* Show your running jobs

			squeue -u staffner

# How do I get set up?

	* Summary of set up

		* Configuration

			* You will need to add elif for your install of the pipeline in the lib/GB.py file.  This is done by copying directly from the lib/GB.py file the lines listed below.  (Make sure to not copy the ones I put in this file because the spacing will be off and Python is very picky about spacing.)  Then paste the copied elif statement as the last elif in that elif.  Replace all of the addresses with addresses to your scratch drive.

				elif '/gpfs/fs2/scratch/acamero7/staffner' in pipeline_full_dir:
					pipeline_folder_loc = '/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline'

					# it is best to put this in a folder which is backed up by bluehive.  Your Documents folder is backed up by bluehive but your scratch drive is not backed up by bluehive. Make sure the folder written below exists before running the pipeline.
					database_backup_folder = '/gpfs/fs2/scratch/acamero7/staffner/archive/micro_pipeline_db_backups'

			* Then make sure the rest of the folders are in the locations below

				ref_folder = pipeline_folder_loc/reference_sequences
				database_file = pipeline_folder_loc/reference_sequences/db/micro_pipeline.db
				MLST_db = pipeline_folder_loc/reference_sequences/db/MLST_db
				blast_db = pipeline_folder_loc/reference_sequences/db/blast_dbs
				plasmid_blast_db = pipeline_folder_loc/reference_sequences/db/plasmid_dbs
				hg19_loc = pipeline_folder_loc/hg19'%(ref_folder)
				hg19_file = pipeline_folder_loc/hg19'%(hg19_loc)
				pipeline_dir = pipeline_folder_loc/micro_pipeline_v2/

		* How to install a reference

			* Make a folder inside the ref_folder with the Genus_species as name (DO NOT INCULDE ANY SPACES IN NAME)

				* Example: /scratch/staffner/test_micro_pipeline/reference_sequences/Pseudomonas_aeruginosa

			* Create a folder inside the one above with the exact same spelling as the one above but include underscore refName

				* Example: /scratch/staffner/test_micro_pipeline/reference_sequences/Pseudomonas_aeruginosa/Pseudomonas_aeruginosa_M18

			* Download .fna and .gbff file from NCBI Genome

				* Example download location: [https://www.ncbi.nlm.nih.gov/genome/187?genome_assembly_id=165503](https://www.ncbi.nlm.nih.gov/genome/187?genome_assembly_id=165503)
			
			* Rename files as to include the exact spelling of the folder above folder with the file extensions

				* Example: 

					* /scratch/staffner/test_micro_pipeline/reference_sequences/Pseudomonas_aeruginosa/Pseudomonas_aeruginosa_M18.gbff.gz
					* /scratch/staffner/test_micro_pipeline/reference_sequences/Pseudomonas_aeruginosa/Pseudomonas_aeruginosa_M18.fna.gz

			* unzip files after transferring to bluehive

				* Example:

					$ cd /scratch/staffner/test_micro_pipeline/reference_sequences/Pseudomonas_aeruginosa/Pseudomonas_aeruginosa_M18
					$ gunzip Pseudomonas_aeruginosa_M18.gbff.gz
					$ gunzip Pseudomonas_aeruginosa_M18.fna.gz

			* Build a bowtie2 index 

				* Example:

						$ module load bowtie2
						$ bowtie2-build Pseudomonas_aeruginosa_M18.fna Pseudomonas_aeruginosa_M18

			* Use [RAST](https://rast.theseed.org/FIG/rast.cgi) to Annotate reference on default settings.

				* download GFF3 (EC numbers stripped) file
				* rename file with genus_species_strain_rast.gff
				* Example: 

					/scratch/staffner/test_micro_pipeline/reference_sequences/Pseudomonas_aeruginosa/Pseudomonas_aeruginosa_M18/Pseudomonas_aeruginosa_M18_rast

			* Download MLST Scheme from [PubMLST](https://pubmlst.org/databases/)

				* Select Genus_Species 
				* Sequence and profile definitions
				* Download MLST profiles and name it MLST_profile.tsv
				* Download Allele Sequences 

			* Install MLST Scheme in Micro_pipeline by following these steps:

				* Add a genus_species folder with the exact same name as the folder for the reference in ref_folder/db/MLST_db

					Example:
						/scratch/staffner/test_micro_pipeline/reference_sequences/db/MLST_db/Pseudomonas_aeruginosa

				* Add MLST profile and Download Allele Sequences to the above folder

					Example:
						$ cd /scratch/staffner/test_micro_pipeline/reference_sequences/db/MLST_db/Pseudomonas_aeruginosa
						$ ls -1
						acsA.fas
						aroE.fas
						guaA.fas
						MLST_profile.tsv
						mutL.fas
						nuoD.fas
						ppsA.fas
						trpE.fas

		* Structure of ref_folder

			* ref_folder

				* Genus_species folder (example: Escherichia_coli)

					* Strain folder (example: Escherichia_coli_MG1655)

						* Escherichia_coli_MG1655_rast.gff - load .fna file into rast download .gff file
						* Escherichia_coli_MG1655.fna - downloaded from refseq

			* Example ref_folder: 

				db
				Enterobacter_aerogenes
				Enterobacter_cloacae
				Escherichia_coli
				hg19
				Klebsiella_oxytoca
				Klebsiella_pneumoniae

				* Example ref_folder/db: 
		
					blast_dbs
					micro_pipeline.db
					MLST_db
					plasmids

					* Example ref_folder/db/blast_dbs (the following files are full of fasta files and make_multi_fasta folder will make DNA and Protein multi fasta files.  These files are required to be a single fasta sequence): 

						AB_resistance_plus
						make_multi_fasta
						ncbi_plasmid
						Pathogenicity_island_db
						plasmidfinder
						resfinder
						serotypefinder
						Virulence_factor_DB
						virulencefinder

# Test install and see if you have the correct permissions

	* Example data is located at either of these locations below

		* Box\micro\about_micro_pipeline\pipeline_backup\samanthas_last_pipeline_backup\example_data_and_run_files
		* /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data/samples_not_assigned_projects

	* Copy example_raw_data folder.  Make sure you are in the same folder that example_raw_data folder is located then run the command below.  This will copy the folder and all of its file contents and make test_install_data

		$ cp -r example_raw_data test_install_data

	* Place the test_install_data folder in the project_data folder

		$ pwd
		/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/URMC_micro_project_data/samples_not_assigned_projects
		$ $ mv test_install_data ../../project_data/test_install_data
		$ cd ../../project_data/
		$ ls
		lib  put_fastq_in_folders.py  test_install_data
		$ cd test_install_data
		$ ls -1
		URMC-774-E-col_examp_S19_L001_R1_001.fastq.gz
		URMC-774-E-col_examp_S19_L001_R2_001.fastq.gz
		URMC-775-E-col_examp_S20_L001_R1_001.fastq.gz
		URMC-775-E-col_examp_S20_L001_R2_001.fastq.gz
		URMC-777-K-pie_examp_S21_L001_R1_001.fastq.gz
		URMC-777-K-pie_examp_S21_L001_R2_001.fastq.gz
		URMC-778-N-men_examp_S22_L001_R1_001.fastq.gz
		URMC-778-N-men_examp_S22_L001_R2_001.fastq.gz

	* Run put_fastq_in_folders.py

		$ cd ..
		$ pwd
		/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/project_data
		$ python put_fastq_in_folders.py -h
		$ python put_fastq_in_folders.py -d test_install_data/
		Making folder of URMC_774_E_col_examp
		Making folder of URMC_775_E_col_examp
		Making folder of URMC_777_K_pie_examp
		Making folder of URMC_774_E_col_examp
		Making folder of URMC_777_K_pie_examp
		Making folder of URMC_778_N_men_examp
		Making folder of URMC_775_E_col_examp
		Making folder of URMC_778_N_men_examp

	* Place in project folders for each species manually using a user interface like WinSCP is easiest. 

		$ cd test_install_data/
		$ ls -1
		E_col
		K_pie
		N_men

	* Now we will only use the E_col project folder going forward
	* Move to the micro_pipeline_v2 folder

		$ cd ../../micro_pipeline_v2/
		$ pwd
		/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2

	* run micro_pipeline_v2.py (make sure you are linking to an out put folder that exists in this example ../results)

		$ python micro_pipeline_v2.py -h
		$ python micro_pipeline_v2.py -refs
		$ python micro_pipeline_v2.py -d ../project_data/test_install_data/E_col/ -o ../results/test_install_ecol -species Escherichia_coli

		###############################
		MICRO PIPELINE UPDATE:
		        2021_07_29 14:19:12
		        All of your samples will now start processing as batch scripts.
		###############################


		Submitted batch job 5900856

		Sample URMC_775_E_col_examp was submitted under job number 5900856
		Submitted batch job 5900857

		Sample URMC_774_E_col_examp was submitted under job number 5900857
		python /gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/micro_pipeline_v2/make_blast_summaries.py -o ../results/test_install_ecol_run_id_1309/run_1309_results/blast_db/blast_hits_by_sample_matrix -d ../results/test_install_ecol_run_id_1309/run_1309_results/blast_db -run_id 1309 -extra_cols None


		sbatch --dependency=afterok:5900856:5900857 ../results/test_install_ecol_run_id_1309/run_1309_results/blast_db/make_hits_matrices.sh
		make_blast_summaries sbatch was submitted under job number 5900858

	* Check to see if it is running (replace staffner with your user name)

		* Currently the SLURM scheduler hasn't started the job yet.

			$ squeue -u staffner
	             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
	           5900856  standard run_id_1 staffner PD       0:00      1 (Priority)
	           5900857  standard run_id_1 staffner PD       0:00      1 (Priority)
	           5900858  standard make_hit staffner PD       0:00      1 (Dependency)

	    * The two jobs 5900856 and 5900857 now have been running for 9:22

		    $ squeue -u staffner
	             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
	           5900858  standard make_hit staffner PD       0:00      1 (Dependency)
	           5900856  standard run_id_1 staffner  R       9:22      1 bhc0091
	           5900857  standard run_id_1 staffner  R       9:22      1 bhc0161

	* Once the run is done the jobs will not longer be present in you squeue

		$ squeue -u staffner
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)

    * Results will be located in the results folder with the folder name you gave it under the -o flag but it will also have a run id added to the folder name.  Every run of the pipeline is given a unique run_id.  Here's my run:

    	/gpfs/fs2/scratch/acamero7/staffner/test_micro_pipeline/results/test_install_ecol_run_id_1309

    * Here's what is inside the folder test_install_ecol_run_id_1309

    	$ ls -1
		jobids_sample.tsv
		run_1309_results
		URMC_774_E_col_examp
		URMC_775_E_col_examp

		* jobids_sample.tsv

			* This records the Bluehive jobids that each sample was run under 

		* run_1309_results

			* This is the folder I copy to box.  This is the folder of interest

		* URMC_774_E_col_examp, URMC_775_E_col_examp

			* These folders contain all of the intermediate processing files and are only useful if something goes wrong.  If you run the pipeline with the flag -do_not_delete_intermediate then these folders become more useful and more strain seeker info about which reference to pick can be found here

# Dependencies (this information is only necessary if moving off of bluehive.  Bluehive has all these dependencies installed.)
	* OS
		* Only can run on a high performance cluster which uses the job scheduler Simple Linux Utility for Resource Management (slurm)

	* Languages/db
		* [python2](https://www.python.org/)
		* [biopython1.70](https://pypi.python.org/pypi/biopython)
		* [perl](https://www.perl.org/)
		* [R](https://www.r-project.org/)
		* [Java Runtime Environment](https://www.java.com/en/)
		* [sqlite3](https://sqlite.org/)


	* micro_pipeline_SNP_calling_v2.py

	* 

		* [Bowtie 2.2.9](http://bowtie-bio.sourceforge.net/bowtie2/)
			* Langmead B, Salzberg S. Fast gapped-read alignment with Bowtie 2. Nature Methods. 2012, 9:357-359 		
		* [samtools 1.5](http://samtools.sourceforge.net/)
			* Li H., Handsaker B., Wysoker A., Fennell T., Ruan J., Homer N., Marth G., Abecasis G., Durbin R. and 1000 Genome Project Data Processing Subgroup (2009) The Sequence alignment/map (SAM) format and SAMtools. Bioinformatics, 25, 2078-9.	
		* [trimmomatic v0.36](http://www.usadellab.org/cms/?page=trimmomatic)
			* Bolger, A. M., Lohse, M., & Usadel, B. (2014). Trimmomatic: A flexible trimmer for Illumina Sequence Data. Bioinformatics, btu170.
		* [fastqc v0.11.5](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
			* Andrews S. (2010). FastQC: a quality control tool for high throughput sequence data. Available online at: http://www.bioinformatics.babraham.ac.uk/projects/fastqc
		* [picard v2.12.0](http://broadinstitute.github.io/picard/)
		* [spades v3.11.1](http://bioinf.spbau.ru/spades)
			* Anton Bankevich, Sergey Nurk, Dmitry Antipov, Alexey A. Gurevich, Mikhail Dvorkin, Alexander S. Kulikov, Valery M. Lesin, Sergey I. Nikolenko, Son Pham, Andrey D. Prjibelski, Alexey V. Pyshkin, Alexander V. Sirotkin, Nikolay Vyahhi, Glenn Tesler, Max A. Alekseyev, and Pavel A. Pevzner. SPAdes: A New Genome Assembly Algorithm and Its Applications to Single-Cell Sequencing. Journal of Computational Biology 19(5) (2012), 455-477.
			* Antipov D., Hartwick N., Shen M., Raiko M., Lapidus A., Pevzner P. A. plasmidSPAdes: Assembling Plasmids from Whole Genome Sequencing Data. Bioinformatics, 2016.
		* [varscan 2.3.9](http://varscan.sourceforge.net/using-varscan.html)
			* Koboldt, D., Zhang, Q., Larson, D., Shen, D., McLellan, M., Lin, L., Miller, C., Mardis, E., Ding, L., & Wilson, R. (2012). VarScan 2: Somatic mutation and copy number alteration discovery in cancer by exome sequencing Genome Research DOI: 10.1101/gr.129684.111
		* [quast v4.5](http://bioinf.spbau.ru/quast)
			* Alexey Gurevich, Vladislav Saveliev, Nikolay Vyahhi and Glenn Tesler, QUAST: quality assessment tool for genome assemblies, Bioinformatics (2013) 29 (8): 1072-1075. doi: 10.1093/bioinformatics/btt086
		* [strainseeker v1.5](http://bioinfo.ut.ee/strainseeker/)
			* Roosaare M, Vaher M, Kaplinski L, Möls M, Andreson R, Lepamets M, Kõressaar T, Naaber P, Kõljalg S, Remm M.  StrainSeeker: fast identification of bacterial strains from raw sequencing readsusing user-provided guide trees. PeerJ. 2017 May 18;5:e3353. doi: 10.7717/peerj.3353. eCollection 2017.
		* [blast](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download)		
		* [picard v2.12.0](https://broadinstitute.github.io/picard/)
		* [raxml v8.2.11](https://sco.h-its.org/exelixis/web/software/raxml/index.html)
		* [bcftools v1.5](https://samtools.github.io/bcftools/bcftools.html)
		* [fasttree v2.1.10](http://www.microbesonline.org/fasttree/)
			* Price MN, Dehal PS, Arkin AP. FastTree 2--approximately maximum-likelihood trees for large alignments. PLoS One. 2010 Mar 10;5(3) 
		* [CFSAN SNP Pipeline v.1.0.0](http://snp-pipeline.readthedocs.io/en/latest/)
			* Davis S, Pettengill JB, Luo Y, Payne J, Shpuntoff A, Rand H, Strain E. (2015) CFSAN SNP Pipeline: an automated method for constructing SNP matrices from next-generation sequence data. PeerJ Computer Science 1:e20
		* [prokka v1.12](http://www.bioinformatics.net.au/software.prokka.shtml)
			* Seemann T. Prokka: rapid prokaryotic genome annotation Bioinformatics 2014 Jul 15;30(14):2068-9
		* [RAST](http://rast.theseed.org/FIG/rast.cgi)
			* The RAST Server: Rapid Annotations using Subsystems Technology. Aziz RK, Bartels D, Best AA, DeJongh M, Disz T, Edwards RA, Formsma K, Gerdes S, Glass EM, Kubal M, Meyer F, Olsen GJ, Olson R, Osterman AL, Overbeek RA, McNeil LK, Paarmann D, Paczian T, Parrello B, Pusch GD, Reich C, Stevens R, Vassieva O, Vonstein V, Wilke A, Zagnitko O. BMC Genomics, 2008


# Version History
* See commit version history on bit bucket 

# Authors

* Samantha Taffner - Software Development
* Adel Malek - Microbiology Expertise 
* Heba Mostafa - Microbiology Expertise
* Jun Wang - NGS Sequencing Expertise
* Nicole Pecora - Microbiology Expertise, NGS Sequencing Expertise, Principal Investigator

# Terminology

* [CIRC](https://circ.rochester.edu/)
* [Bluehive](https://info.circ.rochester.edu/)


  

