# **BWH Micro NGS Pipeline**

# What is it?

	* A pipeline that is very versitale for processing bacterial NGS sequences that can start with fastq, contigs, or nanopore and illumina data.  

# Hardware requirements for storage per sample

	* Each sample once assembled will have multiple intermediate files kept to increase speed for reprocessing

		* ~ 0.4 GB per sample
			* sample_R1.fq.gz
			* sample_R2.fq.gz
			* sample_hg19_filtered_trimmed_R1.fq.gz
			* sample_hg19_filtered_trimmed_R2.fq.gz
			* sample_contigs.fasta
			* sample_plasmid_contigs.fasta

	* Each mapping to a reference will produce intermediate files.  By keeping these files a few hours are saved when a new tree is made mapping to this reference

		* ~ 0.5 GB per mapping
			* reads.all.pileup
			* var.flt.vcf

	* After running step 1 the results files can be very large because blast sequence hits are pulled in multiple different configurations.  Storing these files for long periods of time will add up a lot.  Some ways to reduce size is having the user pick which dbs to run, limit the types of outputs based on hits, and only run species specfic dbs for that species.

# Hardware requirements for processing

	* From assemblied prokka trees, ST typing, and blast.

		* --time=4:00:00

	* nanopore hybrid assembly 

		* --time=48:00:00 --mem=60GB -c 24 

	* STEP 1 without intermediate files. (Spades assembly, blast, st typing, and QC etc)

		* --time=24:00:00 --mem=60GB -c 24	

	* STEP 1 intermediate files produced (blast, st typing, and QC etc)

		* --time=4:00:00 --mem=12GB

	* CFSAN tree

		* map each sample in parallel 

			* -time=24:00:00 --mem=20GB

		* Assemble tree from each individually mapped file.  This incudes making some custom matrices, running trees for each ST type along with the main tree, filtering to make a core genome tree if necessary, and making trees for window filtered and not window filtered.
			
			* --time=72:00:00 --mem=80GB

# Installed data needed for running.  They need an automated way to install them and update them.

	* Custom blast dbs (multi fasta files)

		* I would be good to make in a website a way to for a user to make these dbs and have access and update them.  I ran into many issues with badly formated fasta files 

	* Publicly available dbs (multi fasta files)

		* Need a way to update these and install new dbs

	* References

		* Need a way to install these.  They shouldn't be updated since we have intermedate files mapped to them.  Installing a new one is better.

	* ST typing

		* Need a way to install and update these.  They are updated very often. 

# Custom for species

	* Run software for specific species only.

		* Step 1: Kleborate for Klebsiella 

			* https://www.nature.com/articles/s41467-021-24448-3

	* Some Species need specfic blast databases run.  Currently I run all blast dbs on all samples.  

	* ST typing based on species

# A Step 1 run

	* The user needs a way to add user input like

		optional arguments:
	  -h, --help            show this help message and exit
	  --version             show program's version number and exit
	  -refs                 <flag> Use only this flag if you would like all
	                        possible references printed.
	  -d [INPUT_DIR], --input_dir [INPUT_DIR]
	                        <directory> If multiple samples need to be analyzed a
	                        directory can be included. This directory needs a
	                        specific format. Refer to the bottom of the page for
	                        more information. Turns off the need for -1 and -2
	                        flags.
	  -contig_dir [CONTIG_DIR]
	                        <directory> If multiple contig files need to be
	                        analyzed a directory of contigs can be included. Turns
	                        off the need for -1, -2, or -contig flags.
	  -species [SPECIES]    <str> Include species a species name. The species is
	                        required to be included in possible references already
	                        included. Use -refs flag to list possible refs. Make
	                        sure the space is replaced with _ example
	                        Escherichia_coli.
	  -o [O]                <dir> Directory to save output. Default:
	                        micro_pipeline_output_run_id
	  -trim_window_size [TRIM_WINDOW_SIZE]
	                        <int> Set trimmomatic window size. Specifies the
	                        number of bases to average across. Default is 4
	  -trim_quality [TRIM_QUALITY]
	                        <int> Set trimmomatic sliding window required quality.
	                        Specifies the average quality required. Default is 20
	  -trim_min_len [TRIM_MIN_LEN]
	                        <int> Set trimmomatic minimal length. Specifies the
	                        minimum length of reads to be kept. Default is 100
	  -blast_cut_off [BLAST_CUT_OFF]
	                        <int> Set the blast cut off. Default is 90
	  -len_contig_cutoff [LEN_CONTIG_CUTOFF]
	                        <int> he pipeline will produce a filtered contig file
	                        using both len_contig_cutoff and coverage_min. Prokka
	                        annotation will use this contig file. These values are
	                        reported by the spades assembler and are related to
	                        the assembly. All contigs with an assembly coverage
	                        smaller than coverage_min will be removed from the
	                        file. Default is 10.
	  -coverage_min [COVERAGE_MIN]
	                        <int> The pipeline will produce a filtered contig file
	                        using both len_contig_cutoff and coverage_min. Prokka
	                        annotation will use this contig file. These values are
	                        reported by the spades assembler and are related to
	                        the assembly. All contigs with an assembly coverage
	                        smaller than coverage_min will be removed from the
	                        file. Default is 10.
	  -extra_cols [EXTRA_COLS]
	                        <file> This tag is used to add extra identifiers
	                        columns to matrix. For example add a location column.
	                        To use this tag provide the full address to a csv file
	                        where the first column the sample name followed by any
	                        number of columns are the columns to insert. The first
	                        row needs to be the names of the columns. Do not had
	                        _contigs.fasta to file name.
	  -partition [PARTITION]
	                        <str> Add a run partition to sbatch script.
	  -rm_human_skip        <flag> Use this flag if you would like to skip the
	                        step to remove human reads.
	  -trim_skip            <flag> Use this flag to skip trimming.
	  -mapping_qc_skip      <flag> Use this flag to skip fastqc.
	  -spades_skip          <flag> Use this flag to skip SPAdes assembly.
	  -force_retrim_spades  <flag> Use this flag force the retrimming and
	                        reassembly of a contig file. Normally if these steps
	                        have already been completed on a sample SPAdes is
	                        skipped automatically.
	  -force_spades         <flag> Use this flag force the reassembly of a contig
	                        file. Normally if this step has already been completed
	                        on a sample SPAdes is skipped automatically.
	  -contig_qc_skip       <flag> Use this flag to skip qc of contigs. contig qc
	                        will always be skipped if a new contig file was not
	                        made however, contig qc will be included in report.
	                        contig qc is performed by quast.
	  -strainseeker_skip    <flag> Use this flag to skip strainseeker.
	  -plasmid_contig_skip  <flag> Use this flag to skip assembling plasmids from
	                        contigs. This step is automatically skipped if file is
	                        found in input folder.
	  -blast_db_skip        <flag> Use this flag to skip finding all matches in
	                        blast dbs.
	  -plasmid_blast_db_skip
	                        <flag> Use this flag to skip finding all matches
	                        between plasmid contigs and plasmid blast dbs.
	  -quast_skip           <flag> Use this flag to skip genomic fractions with
	                        quast.
	  -skip_prokka          <flag> Use this flag to skip annotation of contig with
	                        prokka.
	  -MLST_skip            <flag> Use this flag to skip MLST typing.
	  -do_not_delete_intermediate
	                        <flag> Use this flag so the intermediate processing
	                        files are not deleted.

# tree run user previously had these options

	optional arguments:
	  -h, --help            show this help message and exit
	  --version             show program's version number and exit
	  -refs                 <flag> Use only this flag if you would like all
	                        possible references printed.
	  -ref_info             <flag> Use only this flag to describe more more
	                        information about the reference
	  -d [INPUT_DIR], --input_dir [INPUT_DIR]
	                        <directory> STEP MAP ONLY: All samples folders should
	                        be nested in the same folder. Submit the parent folder
	                        to this program. This directory needs a specific
	                        format. Refer to the bottom of the page for more
	                        information.
	  -substrain [SUBSTRAIN]
	                        <str> Include the substrain which would like to be
	                        used as the reference. The substrain is required to be
	                        included in possible references already included. Use
	                        -refs flag to list possible refs. Make sure the space
	                        is replaced with _ example Escherichia_coli_MG1655.
	                        Example: -substrain ../reference_sequences/Escherichia
	                        _coli/Escherichia_coli_MG1655/Escherichia_coli_MG1655
	  -o [O]                <dir> REQUIRED BOTH STEPS: Directory to save output.
	  -window_size [WINDOW_SIZE]
	                        <int> The length of the window in which the number of
	                        SNPs should be no more than max_num_snp. Default is 50
	  -max_num_snps [MAX_NUM_SNPS]
	                        <int> The maximum number of SNPs allowed in a window.
	                        Default is 3
	  -ignore_samples [IGNORE_SAMPLES]
	                        <file> Pass a file which is a list of sample names to
	                        ignore in analysis. Make sure the sample name is the
	                        exact same as the name of the folder that samples are
	                        saved in. This only works for the main tree. Default
	                        is off.
	  -sample_subset [SAMPLE_SUBSET]
	                        <file> Pass a file which is a list of sample names to
	                        run in the tree. This is only necessary if you would
	                        like to run a subset of samples. Make sure the sample
	                        name is the exact same as the name of the folder that
	                        samples are saved in. Each sample needs to be on a
	                        newline. Default is off.
	  -force_remap          <flag> Use this flag force the remapping of data for
	                        the input reference. Normally intermediate mapping
	                        files are saved for each reference the sample was
	                        mapped to in the sample folder.
	  -filter_regions       <flag> Turn off filtering by a window and number of
	                        snps in window. Use tags -window_size and
	                        -max_num_snps to change filtering criteria.
	  -cov_snp_app          <flag> Use this flag to turn on the SNP coverage app.
	                        This app will provide a local webpage to view the
	                        coverage of each SNP. This function will only run if
	                        you have less than 50 samples in your tree. Currently
	                        this option is set to not run automatically because
	                        there is a gut on line 1162 where the list index is
	                        out of range.
	  -MLST_trees           <flag> Turn off making MLST based trees. If left on it
	                        will make a tree for each individual ST type. Default
	                        is on.
	  -rm_phage             <flag> Turn on removing snps in phages, mobile
	                        elements, and transposons. Default is off.
	  -rm_nulls             <flag> Turn on removing nulls. Default is off.
	  -mapping_type [MAPPING_TYPE]
	                        <str> Use to choose type of mapping. Default is
	                        unpaired_bowtie2. Other options include
	                        paired_bowtie2.
	  -vis_skip             <flag> Use this flag to skip making visualization.
	                        THIS IS REQUIRED IF GENOME IS NOT FINISHED